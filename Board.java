import java.util.ArrayList;
import java.util.HashMap;

// Board шахматная доска
public class Board {
    private static final HashMap<Integer, String> ordinateMap = new HashMap<Integer, String>() {{
        put(1, "a");
        put(2, "b");
        put(3, "c");
        put(4, "d");
        put(5, "e");
        put(6, "f");
        put(7, "g");
        put(8, "h");
    }};

    private final ArrayList<Figure> figures;

    public Board() {
        this.figures = new ArrayList<>();
    }

    public Board(ArrayList<Figure> figures) {
        this.figures = figures;
    }

    public Board copy() {
        ArrayList<Figure> figures = new ArrayList<>(this.figures);
        return new Board(figures);
    }

    public void addFigure(Figure figure) {
        figures.add(figure);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Figure f : figures) {
            sb.append(String.format("%s%s ", f.x+1, ordinateMap.get(f.y+1)));
        }
        return sb.toString();
    }

    public boolean isAttacked(Figure figure) {
        for (Figure f : figures) {
            if (f.isAttack(figure)) {
                return true;
            }
            if (figure.isAttack(f)) {
                return true;
            }
        }
        return false;
    }
}
