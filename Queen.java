
// Queen шахматная королева
public class Queen extends Figure {

    public Queen(int x, int y) {
        super(x, y);
    }

    @Override
    public boolean isAttack(Figure f) {
        // Атака по горизонтали
        if (this.x == f.x) {
            return true;
        }
        // Атака по вертикали
        if (this.y == f.y) {
            return true;
        }
        // Атака по диагонали
        return Math.abs(this.x - f.x) == (Math.abs(this.y - f.y));
    }
}
