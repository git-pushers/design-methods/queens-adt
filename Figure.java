
// Figure класс шахматной фигуры
public abstract class Figure {
    int x;
    int y;

    public abstract boolean isAttack(Figure f);

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
