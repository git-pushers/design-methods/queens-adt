### Eight Queens (8Q)
Method 1. Abstract Data Types

### Example
```shell
javac Main.java
java Main.java
```

### Comparison
| Criteria/Solution                   | Abstract data type (Base)                                                                                                   | Pipes-and-filters (Alternative)                                                                     |
|-------------------------------------|-----------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------|
| Change the implementation algorithm | + ADT provides more stable classes.  The main part of the algorithm  that controls ADT is short and simple.                 | - Hard to decompose that algorithm  into sequence of discrete actions naturally.                    |
| Change data representation          | + It will be easy to do if domain invariants  are unchanged                                                                 | - It can be difficult, because all filters use  one shared structure to communicate with each other |
| Perfomance                          | + Natural algorithm                                                                                                         | - Non-effecient sequence of actions                                                                 |
| Reuse                               | ADTs are based on stable domain. We can another chess figure and make more complicated changes without destroyed refactoring |                                                                                                     |