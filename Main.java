import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Board> boards = new ArrayList<>();
        boards.add(new Board());

        for (int x = 0; x < 8; x++) {
            ArrayList<Board> nextBoards = new ArrayList<>();
            for (int y = 0; y < 8; y++) {
                Queen queen = new Queen(x, y);
                for (Board b : boards) {
                    if (!b.isAttacked(queen)) {
                        Board newBoard = b.copy();
                        newBoard.addFigure(queen);
                        nextBoards.add(newBoard);
                    }
                }
            }
            boards = nextBoards;
        }

        System.out.format("%d different solutions (not considering symmetry)\n", boards.size());
        for (int i = 0; i < boards.size(); i++) {
            System.out.format("#%d solution\n", i + 1);
            System.out.println(boards.get(i));
        }

    }

}
